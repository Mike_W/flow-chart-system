﻿namespace work1
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.graphic = new System.Windows.Forms.ToolStripMenuItem();
            this.start = new System.Windows.Forms.ToolStripMenuItem();
            this.in_out = new System.Windows.Forms.ToolStripMenuItem();
            this.address = new System.Windows.Forms.ToolStripMenuItem();
            this.judge = new System.Windows.Forms.ToolStripMenuItem();
            this.connect = new System.Windows.Forms.ToolStripMenuItem();
            this.generateCode = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.edit = new System.Windows.Forms.ToolStripMenuItem();
            this.delete = new System.Windows.Forms.ToolStripMenuItem();
            this.run = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.deleteMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.graphic,
            this.generateCode,
            this.run});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(749, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // graphic
            // 
            this.graphic.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.start,
            this.in_out,
            this.address,
            this.judge,
            this.connect});
            this.graphic.Name = "graphic";
            this.graphic.Size = new System.Drawing.Size(53, 24);
            this.graphic.Text = "图元";
            // 
            // start
            // 
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(167, 26);
            this.start.Text = "起始框";
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // in_out
            // 
            this.in_out.Name = "in_out";
            this.in_out.Size = new System.Drawing.Size(167, 26);
            this.in_out.Text = "输入输出框";
            this.in_out.Click += new System.EventHandler(this.in_out_Click);
            // 
            // address
            // 
            this.address.Name = "address";
            this.address.Size = new System.Drawing.Size(167, 26);
            this.address.Text = "处理框";
            this.address.Click += new System.EventHandler(this.address_Click);
            // 
            // judge
            // 
            this.judge.Name = "judge";
            this.judge.Size = new System.Drawing.Size(167, 26);
            this.judge.Text = "判断框";
            this.judge.Click += new System.EventHandler(this.judge_Click);
            // 
            // connect
            // 
            this.connect.Name = "connect";
            this.connect.Size = new System.Drawing.Size(167, 26);
            this.connect.Text = "连接点";
            this.connect.Click += new System.EventHandler(this.connect_Click);
            // 
            // generateCode
            // 
            this.generateCode.Name = "generateCode";
            this.generateCode.Size = new System.Drawing.Size(83, 24);
            this.generateCode.Text = "生成代码";
            this.generateCode.Click += new System.EventHandler(this.generateCode_Click);
            // 
            // deleteMenuStrip
            // 
            this.deleteMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.deleteMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.edit,
            this.delete});
            this.deleteMenuStrip.Name = "deleteMenuStrip";
            this.deleteMenuStrip.Size = new System.Drawing.Size(139, 52);
            this.deleteMenuStrip.Text = "操作";
            // 
            // edit
            // 
            this.edit.Name = "edit";
            this.edit.Size = new System.Drawing.Size(138, 24);
            this.edit.Text = "编辑文本";
            // 
            // delete
            // 
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(138, 24);
            this.delete.Text = "删除";
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // run
            // 
            this.run.Name = "run";
            this.run.Size = new System.Drawing.Size(53, 24);
            this.run.Text = "执行";
            this.run.Click += new System.EventHandler(this.run_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(749, 478);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "Form1";
            this.Text = "程序流程图系统";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDoubleClick);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseHover += new System.EventHandler(this.Form1_MouseHover);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.deleteMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem graphic;
        private System.Windows.Forms.ToolStripMenuItem start;
        private System.Windows.Forms.ToolStripMenuItem in_out;
        private System.Windows.Forms.ToolStripMenuItem address;
        private System.Windows.Forms.ToolStripMenuItem judge;
        private System.Windows.Forms.ToolStripMenuItem connect;
        private System.Windows.Forms.ContextMenuStrip deleteMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem edit;
        private System.Windows.Forms.ToolStripMenuItem delete;
        private System.Windows.Forms.ToolStripMenuItem generateCode;
        private System.Windows.Forms.ToolStripMenuItem run;
    }
}

