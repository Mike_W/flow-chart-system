﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace work1
{
    public class CodeVisitor : Visitor

    {
        FileStream fs = new FileStream("E:\\work\\work1\\target.txt",FileMode.OpenOrCreate,FileAccess.ReadWrite,FileShare.ReadWrite);
        public void Begin()
        {
            Byte[] data= System.Text.Encoding.Default.GetBytes("public class Generate {"+"\n");
            fs.Write(data, 0, data.Length);
            fs.Flush();
        }
        public void Visit(MyRectangle myRectangle, Graphics g)
        {
            if(myRectangle.LineList[1].EndGrapthic.GetType()==typeof(Diamond)&& Judge((Diamond)myRectangle.LineList[1].EndGrapthic) == 3)
            {
                myRectangle.LineList[1].EndGrapthic.Accept(this,g);
            }
            else
            {
                string str = myRectangle.Letter;
                string[] data = str.Split(';');
                for (int i = 0; i < data.Length; i++)
                {
                    byte[] res = System.Text.Encoding.Default.GetBytes("  " + data[i] + ";\n");
                    fs.Write(res, 0, res.Length);
                    fs.Flush();
                }
                if (myRectangle.LineList.Count >= 2)
                {
                    myRectangle.LineList[1].EndGrapthic.Accept(this,g);
                }
            }
            
        }
        public int Judge(Diamond diamond)
        {
            for(int i = 0; i < diamond.LineList.Count; i++)
            {
                if (diamond.LineList[i].Letter != null)
                {
                    if (diamond.LineList[i].Letter.Equals("Y"))
                    {
                        //分支结构
                        if (diamond.LineList[i].EndGrapthic.LineList[1].EndGrapthic.GetType() == typeof(Circle))
                        {
                            return 1;
                        }
                        //while循环
                        else if (diamond.LineList[i].EndGrapthic.LineList[1].EndGrapthic.GetType() == typeof(Diamond))
                        {
                            return 2;
                        }
                        //for循环
                        else if (diamond.LineList[i].EndGrapthic.LineList[1].EndGrapthic.GetType() == typeof(MyRectangle)&& diamond.LineList[i].EndGrapthic.LineList[1].EndGrapthic.LineList[1].EndGrapthic.GetType()==typeof(Diamond))
                        {
                            return 3;
                        }

                    }
                }
                
            }
            return 0;
        }
        public void Visit(Diamond diamond, Graphics g)
        {
            int res = Judge(diamond);
            if (res == 1)
            {
                
                byte[] res1 = System.Text.Encoding.Default.GetBytes("  " + "if(" + diamond.Letter + "){\n");
                fs.Write(res1, 0, res1.Length);
                fs.Flush();
                for (int i = 0; i < diamond.LineList.Count; i++)
                {
                    if (diamond.LineList[i].Letter != null)
                    {
                        if (diamond.LineList[i].Letter.Equals("Y"))
                        {
                            string str = diamond.LineList[i].EndGrapthic.Letter;
                            string[] data = str.Split(';');
                            for (int j = 0; j < data.Length; j++)
                            {
                                if (data[j] != " ")
                                {
                                    byte[] res2 = System.Text.Encoding.Default.GetBytes("  " + data[j] + ";\n");
                                    fs.Write(res2, 0, res2.Length);
                                    fs.Flush();
                                }
                                   
                            }

                        }
                    }
                }
                byte[] res3 = System.Text.Encoding.Default.GetBytes("}else{\n");
                fs.Write(res3, 0, res3.Length);

                fs.Flush();
                for (int i = 0; i < diamond.LineList.Count; i++)
                {
                    if (diamond.LineList[i].Letter != null)
                    {
                        if (diamond.LineList[i].Letter.Equals("N"))
                        {
                            string str = diamond.LineList[i].EndGrapthic.Letter;
                            string[] data = str.Split(';');     
                            for (int j = 0; j < data.Length; j++)
                            {
                                if(data[j]!=" ")
                                {
                                    byte[] res2 = System.Text.Encoding.Default.GetBytes("  " + data[j] + ";\n");
                                    fs.Write(res2, 0, res2.Length);
                                    fs.Flush();
                                }
                                
                            }

                        }
                    }
                }
                byte[] res5 = System.Text.Encoding.Default.GetBytes("}\n");
                fs.Write(res5, 0, res5.Length);
                fs.Flush();
                if (diamond.LineList[1].EndGrapthic.LineList[1].EndGrapthic.LineList.Count >= 3)
                {
                   
                    for (int i=0;i< diamond.LineList[1].EndGrapthic.LineList[1].EndGrapthic.LineList.Count; i++)
                    {
                        if (diamond.LineList[1].EndGrapthic.LineList[1].EndGrapthic.isConnected(diamond.LineList[1].EndGrapthic.LineList[1].EndGrapthic.LineList[i].StartPoint)!=0)
                        {
                            diamond.LineList[1].EndGrapthic.LineList[1].EndGrapthic.LineList[i].EndGrapthic.Accept(this,g);
                        }
                    }
                }
            }
            if (res == 2)
            {
                byte[] res1 = System.Text.Encoding.Default.GetBytes("  " + "while("+diamond.Letter+"){\n");
                fs.Write(res1, 0, res1.Length);
                fs.Flush();
                for (int i = 0; i < diamond.LineList.Count; i++)
                {
                    if (diamond.LineList[i].Letter != null)
                    {
                        if (diamond.LineList[i].Letter.Equals("Y"))
                        {
                            string str = diamond.LineList[i].EndGrapthic.Letter;
                            string[] data = str.Split(';');
                            for (int j = 0; j < data.Length; j++)
                            {
                                byte[] res2 = System.Text.Encoding.Default.GetBytes("  " + data[j] + ";\n");
                                fs.Write(res2, 0, res2.Length);
                                fs.Flush();
                            }

                        }
                    }
                        
                }
                byte[] res3 = System.Text.Encoding.Default.GetBytes("}\n");
                fs.Write(res3, 0, res3.Length);
                fs.Flush();
                for (int i = 0; i < diamond.LineList.Count; i++)
                {
                    if (diamond.LineList[i].Letter != null)
                    {
                        if (diamond.LineList[i].Letter.Equals("N"))
                        {
                            diamond.LineList[i].EndGrapthic.Accept(this,g);
                        }
                    }
                        
                }
            }
            if (res == 3)
            {
                Console.WriteLine(diamond.LineList[0].StartGraohic.Letter);
                byte[] res1 = System.Text.Encoding.Default.GetBytes("  " + "for(" + diamond.LineList[0].StartGraohic.Letter + ";" + diamond.Letter+";") ;
                fs.Write(res1, 0, res1.Length);
                fs.Flush();
                for (int i = 0; i < diamond.LineList.Count; i++)
                {
                    if (diamond.LineList[i].Letter != null)
                    {
                        if (diamond.LineList[i].Letter.Equals("Y"))
                        {
                            string str1 = diamond.LineList[i].EndGrapthic.LineList[1].EndGrapthic.Letter + "){\n";
                            byte[] res4 = System.Text.Encoding.Default.GetBytes(str1);
                            fs.Write(res4, 0, res4.Length);
                            fs.Flush();
                            string str = diamond.LineList[i].EndGrapthic.Letter;
                            string[] data = str.Split(';');
                            for (int j = 0; j < data.Length; j++)
                            {
                                byte[] res2 = System.Text.Encoding.Default.GetBytes("  " + data[j] + ";\n");
                                fs.Write(res2, 0, res2.Length);
                                fs.Flush();
                            }

                        }
                    }

                }
                byte[] res3 = System.Text.Encoding.Default.GetBytes("}\n");
                fs.Write(res3, 0, res3.Length);
                fs.Flush();
                for (int i = 0; i < diamond.LineList.Count; i++)
                {
                    if (diamond.LineList[i].Letter != null)
                    {
                        if (diamond.LineList[i].Letter.Equals("N"))
                        {
                            diamond.LineList[i].EndGrapthic.Accept(this,g);
                        }
                    }

                }
            }
        }

        public void Visit(Parallelogram parallelogram, Graphics g)
        {
            if (parallelogram.Letter.Contains("输入"))
            {
                string str = parallelogram.Letter;
                string subStr = str.Substring(2);
                string[] data = subStr.Split(',');
                for (int i = 0; i < data.Length; i++)
                {
                    if (data[i].Contains("整形"))
                    {
                        string tempData = data[i].Substring(2);
                        byte[] res1 = System.Text.Encoding.Default.GetBytes("  public int "+tempData+ ";\n");
                        fs.Write(res1, 0, res1.Length);              
                        fs.Flush();
                    }
                    if (data[i].Contains("布尔"))
                    {
                        string tempData = data[i].Substring(2);
                        byte[] res1 = System.Text.Encoding.Default.GetBytes("  public boolean " + tempData + ";\n");
                        fs.Write(res1, 0, res1.Length);
                        fs.Flush();
                    }
                    if (data[i].Contains("字符串"))
                    {
                        string tempData = data[i].Substring(3);
                        byte[] res1 = System.Text.Encoding.Default.GetBytes("  public String " + tempData + ";\n");
                        fs.Write(res1, 0, res1.Length) ;
                        fs.Flush();
                    }
                }
                if (parallelogram.LineList.Count >= 2)
                {
                    parallelogram.LineList[1].EndGrapthic.Accept(this,g);
                }
            }
            else if (parallelogram.Letter.Contains("输出"))
            {
                string str = parallelogram.Letter;
                string subStr = str.Substring(2);
                string[] data = subStr.Split(',');
                for (int i = 0; i < data.Length; i++)
                {
                    Console.WriteLine(data[i]);
                    if(data[i]!=" ")
                    {
                        byte[] res = System.Text.Encoding.Default.GetBytes("  System.out.println(" + data[i] + ");\n");
                        fs.Write(res, 0, res.Length);
                        fs.Flush();
                    }
                   
                }
                if (parallelogram.LineList.Count >= 2)
                {
                    parallelogram.LineList[1].EndGrapthic.Accept(this,g);
                }
            }
        }

        public void Visit(Ellipse ellipse, Graphics g)
        {
            String str = ellipse.Letter;
            if (str.Equals("开始"))
            {
                if (ellipse.LineList.Count > 0)
                {
                    ellipse.LineList[0].EndGrapthic.Accept(this,g);
                }
            }
            else if (str.Equals("结束"))
            {
                Byte[] data = System.Text.Encoding.Default.GetBytes("}" + "\n");
                fs.Write(data, 0, data.Length);
                fs.Flush();
                fs.Close();
               
            }
        }

        public void Visit(Circle circle, Graphics g)
        {
            throw new NotImplementedException();
        }
    }
}
