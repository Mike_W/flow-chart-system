﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace work1
{
    [Serializable]
    public class Line
    {

        private Point point1;
        private Point point2;
        private Point point3;
        private Point point4;
        private Point point5;
        private Point point6;
        private Point startPoint;
        private Point endPoint;
        private BaseGrapthic startGraohic;
        private BaseGrapthic endGrapthic;
        private string letter;
        private RectangleF rectangleF;
        public Point Point1 { get => point1; set => point1 = value; }
        public Point Point2 { get => point2; set => point2 = value; }
        public Point Point3 { get => point3; set => point3 = value; }
        public Point Point4 { get => point4; set => point4 = value; }
        public Point Point5 { get => point5; set => point5 = value; }
        public Point Point6 { get => point6; set => point6 = value; }
        public Point StartPoint { get => startPoint; set => startPoint = value; }
        public Point EndPoint { get => endPoint; set => endPoint = value; }
        public BaseGrapthic StartGraohic { get => startGraohic; set => startGraohic = value; }
        public BaseGrapthic EndGrapthic { get => endGrapthic; set => endGrapthic = value; }
        public string Letter { get => letter; set => letter = value; }
        public RectangleF RectangleF { get => rectangleF; set => rectangleF = value; }

        public void drawString(Graphics g)
        {
            Font font = new Font(new FontFamily("Times new roman"), 12);
            Brush brush = new SolidBrush(Color.Black);
            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;
            g.DrawString(this.Letter, font, brush,this.RectangleF, stringFormat);
        }
        public bool contains(int x,int y)
        {
            if (this.Point1.X == this.point2.X)
            {
                if (Math.Abs(y - this.point1.Y) < Math.Abs(this.point1.Y - this.point2.Y)&&Math.Abs(x-this.point1.X)<10&&!this.startGraohic.contains(x,y))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            if (this.Point1.Y == this.point2.Y)
            {
                if (Math.Abs(x - this.point1.X) < Math.Abs(this.point1.X - this.point2.X)&& Math.Abs(y-this.point1.Y)<10 && !this.startGraohic.contains(x, y))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else return false;
            
        }
        //第一个点鼠标样式
        public void move1(Point rigid, MouseEventArgs anim, Graphics g)
        {
            Point lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            Pen pen = new Pen(Color.Black, 2);
            Pen pen1 = new Pen(Color.Black, 2);
            pen.EndCap = LineCap.Custom;
            pen.CustomEndCap = new AdjustableArrowCap(5f, 5f, true);
            if (Math.Abs(anim.X - rigid.X) < 3 && (rigid.Y - anim.Y) > 20)
            {
                lineP2 = anim.Location;

                g.DrawLine(pen, rigid, lineP2);
            }
            //向上折线样式
            else if (Math.Abs(anim.X - rigid.X) < (rigid.Y - anim.Y) && (rigid.Y - anim.Y) > 20)
            {
                lineP2.X = rigid.X;
                lineP2.Y = rigid.Y - (rigid.Y - anim.Y) / 2;
                lineP3.X = anim.X;
                lineP3.Y = lineP2.Y;
                lineP4 = anim.Location;
                g.DrawLine(pen1, rigid, lineP2);
                g.DrawLine(pen1, lineP2, lineP3);
                g.DrawLine(pen, lineP3, lineP4);
            }
            //向上折线样式
            else if ((Math.Abs(anim.X - rigid.X) > (rigid.Y - anim.Y) && (rigid.Y - anim.Y) > 20))
            {
                lineP2.X = rigid.X;
                lineP2.Y = anim.Y;
                lineP3 = anim.Location;
                g.DrawLine(pen1, rigid, lineP2);
                g.DrawLine(pen, lineP2, lineP3);
            }
            //向下折线样式
            else if (Math.Abs(anim.X - rigid.X) > Math.Abs(rigid.Y - anim.Y) && (anim.Y - rigid.Y) > 20)
            {
                if (anim.X > rigid.X)
                {
                    lineP2.X = rigid.X;
                    lineP2.Y = rigid.Y - 20;
                    lineP3.X = anim.X - 20;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = anim.Y;
                    lineP5 = anim.Location;
                    g.DrawLine(pen1, rigid, lineP2);
                    g.DrawLine(pen1, lineP2, lineP3);
                    g.DrawLine(pen1, lineP3, lineP4);
                    g.DrawLine(pen, lineP4, lineP5);
                }
                else if (anim.X < rigid.X)
                {
                    lineP2.X = rigid.X;
                    lineP2.Y = rigid.Y - 20;
                    lineP3.X = anim.X + 20;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = anim.Y;
                    lineP5 = anim.Location;
                    g.DrawLine(pen1, rigid, lineP2);
                    g.DrawLine(pen1, lineP2, lineP3);
                    g.DrawLine(pen1, lineP3, lineP4);
                    g.DrawLine(pen, lineP4, lineP5);
                }
            }
            //向下折线样式
            else if (Math.Abs(anim.X - rigid.X) < Math.Abs(rigid.Y - anim.Y) && (anim.Y - rigid.Y) > 20)
            {
                if (Math.Abs(anim.X - rigid.X) > 65)
                {
                    lineP2.X = rigid.X;
                    lineP2.Y = rigid.Y - 20;
                    lineP3.X = anim.X;
                    lineP3.Y = lineP2.Y;
                    lineP4 = anim.Location;
                    g.DrawLine(pen1, rigid, lineP2);
                    g.DrawLine(pen1, lineP2, lineP3);
                    g.DrawLine(pen, lineP3, lineP4);
                }
                else if (Math.Abs(anim.X - rigid.X) < 65)
                {

                    lineP2.X = rigid.X;
                    lineP2.Y = rigid.Y - 20;
                    if (anim.X > rigid.X)
                    {
                        lineP3.X = (int)(rigid.X + 65);
                    }
                    else if (anim.X < rigid.X)
                    {
                        lineP3.X = (int)(rigid.X - 65);
                    }
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = anim.Y - 20;
                    lineP5.X = anim.X;
                    lineP5.Y = lineP4.Y;
                    lineP6 = anim.Location;
                    g.DrawLine(pen1, rigid, lineP2);
                    g.DrawLine(pen1, lineP2, lineP3);
                    g.DrawLine(pen1, lineP3, lineP4);
                    g.DrawLine(pen1, lineP4, lineP5);
                    g.DrawLine(pen, lineP5, lineP6);
                }
            }
        }
        //第二个点鼠标样式
        public void move2(Point rigid, MouseEventArgs anim, Graphics g)
        {
            Point lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            Pen pen = new Pen(Color.Black, 2);
            Pen pen1 = new Pen(Color.Black, 2);
            pen.EndCap = LineCap.Custom;
            pen.CustomEndCap = new AdjustableArrowCap(5f, 5f, true);
            if (anim.X > rigid.X + 10)
            {
                if (Math.Abs(anim.X - rigid.X) > Math.Abs(anim.Y - rigid.Y))
                {
                    lineP2.X = rigid.X + (anim.X - rigid.X) / 2;
                    lineP2.Y = rigid.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = anim.Y;
                    g.DrawLine(pen1, rigid, lineP2);
                    g.DrawLine(pen1, lineP2, lineP3);
                    g.DrawLine(pen, lineP3, anim.Location);
                }
                else
                {
                    lineP2.X = anim.X;
                    lineP2.Y = rigid.Y;
                    g.DrawLine(pen1, rigid, lineP2);
                    g.DrawLine(pen, lineP2, anim.Location);
                }
            }
            else if (anim.X < rigid.X + 10)
            {
                if (Math.Abs(anim.Y - rigid.Y) > 50)
                {
                    if (Math.Abs(anim.X - rigid.X) > Math.Abs(anim.Y - rigid.Y))
                    {
                        lineP2.X = rigid.X + 30;
                        lineP2.Y = rigid.Y;
                        lineP3.X = lineP2.X;
                        lineP3.Y = anim.Y;
                        g.DrawLine(pen1, rigid, lineP2);
                        g.DrawLine(pen1, lineP2, lineP3);
                        g.DrawLine(pen, lineP3, anim.Location);
                    }
                    else if (Math.Abs(anim.X - rigid.X) < Math.Abs(anim.Y - rigid.Y))
                    {
                        lineP2.X = rigid.X + 30;
                        lineP2.Y = rigid.Y;
                        lineP3.X = lineP2.X;
                        if (anim.Y < rigid.Y)
                            lineP3.Y = anim.Y + 20;
                        else
                            lineP3.Y = anim.Y - 20;
                        lineP4.X = anim.X;
                        lineP4.Y = lineP3.Y;
                        g.DrawLine(pen1, rigid, lineP2);
                        g.DrawLine(pen1, lineP2, lineP3);
                        g.DrawLine(pen1, lineP3, lineP4);
                        g.DrawLine(pen, lineP4, anim.Location);
                    }
                }
                else
                {
                    lineP2.X = rigid.X + 30;
                    lineP2.Y = rigid.Y;
                    lineP3.X = lineP2.X;
                    if (anim.Y > rigid.Y)
                        lineP3.Y = rigid.Y + 50;
                    else
                        lineP3.Y = rigid.Y - 50;
                    lineP4.X = anim.X + 20;
                    lineP4.Y = lineP3.Y;
                    lineP5.X = lineP4.X;
                    lineP5.Y = anim.Y;
                    g.DrawLine(pen1, rigid, lineP2);
                    g.DrawLine(pen1, lineP2, lineP3);
                    g.DrawLine(pen1, lineP3, lineP4);
                    g.DrawLine(pen1, lineP4, lineP5);
                    g.DrawLine(pen, lineP5, anim.Location);
                }
            }
        }
        public void move3(Point rigid, MouseEventArgs anim, Graphics g)
        {
            Point lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            Pen pen = new Pen(Color.Black, 2);
            Pen pen1 = new Pen(Color.Black, 2);
            pen.EndCap = LineCap.Custom;
            pen.CustomEndCap = new AdjustableArrowCap(5f, 5f, true);
            if (anim.Y > rigid.Y)
            {
                if (Math.Abs(anim.X - rigid.X) > Math.Abs(anim.Y - rigid.Y))
                {
                    lineP2.X = rigid.X;
                    lineP2.Y = anim.Y;
                    g.DrawLine(pen1, rigid, lineP2);
                    g.DrawLine(pen, lineP2, anim.Location);
                }
                else
                {
                    lineP2.X = rigid.X;
                    lineP2.Y = rigid.Y + (anim.Y - rigid.Y) / 2;
                    lineP3.X = anim.X;
                    lineP3.Y = lineP2.Y;
                    g.DrawLine(pen1, rigid, lineP2);
                    g.DrawLine(pen1, lineP2, lineP3);
                    g.DrawLine(pen, lineP3, anim.Location);
                }
            }
            else
            {
                if (Math.Abs(anim.X - rigid.X) > 65)
                {
                    if (Math.Abs(anim.X - rigid.X) < Math.Abs(anim.Y - rigid.Y))
                    {
                        lineP2.X = rigid.X;
                        lineP2.Y = rigid.Y + 20;
                        lineP3.X = anim.X;
                        lineP3.Y = lineP2.Y;
                        g.DrawLine(pen1, rigid, lineP2);
                        g.DrawLine(pen1, lineP2, lineP3);
                        g.DrawLine(pen, lineP3, anim.Location);
                    }
                    else
                    {
                        lineP2.X = rigid.X;
                        lineP2.Y = rigid.Y + 20;
                        if (anim.X < rigid.X)
                            lineP3.X = anim.X + 20;
                        else
                            lineP3.X = anim.X - 20;
                        lineP3.Y = lineP2.Y;
                        lineP4.X = lineP3.X;
                        lineP4.Y = anim.Y;
                        g.DrawLine(pen1, rigid, lineP2);
                        g.DrawLine(pen1, lineP2, lineP3);
                        g.DrawLine(pen1, lineP3, lineP4);
                        g.DrawLine(pen, lineP4, anim.Location);
                    }
                }
                else
                {
                    lineP2.X = rigid.X;
                    lineP2.Y = rigid.Y + 20;
                    if (anim.X < rigid.X)
                        lineP3.X = rigid.X - 65;
                    else
                        lineP3.X = rigid.X + 65;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = anim.Y + 20;
                    lineP5.X = anim.X;
                    lineP5.Y = lineP4.Y;
                    g.DrawLine(pen1, rigid, lineP2);
                    g.DrawLine(pen1, lineP2, lineP3);
                    g.DrawLine(pen1, lineP3, lineP4);
                    g.DrawLine(pen1, lineP4, lineP5);
                    g.DrawLine(pen, lineP5, anim.Location);
                }
            }
        }
        public void move4(Point rigid, MouseEventArgs anim, Graphics g)
        {
            Point lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            Pen pen = new Pen(Color.Black, 2);
            Pen pen1 = new Pen(Color.Black, 2);
            pen.EndCap = LineCap.Custom;
            pen.CustomEndCap = new AdjustableArrowCap(5f, 5f, true);
            if (anim.X < rigid.X - 10)
            {
                if (Math.Abs(anim.X - rigid.X) > Math.Abs(anim.Y - rigid.Y))
                {
                    lineP2.X = rigid.X - (rigid.X - anim.X) / 2;
                    lineP2.Y = rigid.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = anim.Y;
                    g.DrawLine(pen1, rigid, lineP2);
                    g.DrawLine(pen1, lineP2, lineP3);
                    g.DrawLine(pen, lineP3, anim.Location);
                }
                else
                {
                    lineP2.X = anim.X;
                    lineP2.Y = rigid.Y;
                    g.DrawLine(pen1, rigid, lineP2);
                    g.DrawLine(pen, lineP2, anim.Location);
                }
            }
            else if (anim.X > rigid.X - 10)
            {
                if (Math.Abs(anim.Y - rigid.Y) > 50)
                {
                    if (Math.Abs(anim.X - rigid.X) > Math.Abs(anim.Y - rigid.Y))
                    {
                        lineP2.X = rigid.X - 30;
                        lineP2.Y = rigid.Y;
                        lineP3.X = lineP2.X;
                        lineP3.Y = anim.Y;
                        g.DrawLine(pen1, rigid, lineP2);
                        g.DrawLine(pen1, lineP2, lineP3);
                        g.DrawLine(pen, lineP3, anim.Location);
                    }
                    else if (Math.Abs(anim.X - rigid.X) < Math.Abs(anim.Y - rigid.Y))
                    {
                        lineP2.X = rigid.X - 30;
                        lineP2.Y = rigid.Y;
                        lineP3.X = lineP2.X;
                        if (anim.Y < rigid.Y)
                            lineP3.Y = anim.Y + 20;
                        else
                            lineP3.Y = anim.Y - 20;
                        lineP4.X = anim.X;
                        lineP4.Y = lineP3.Y;
                        g.DrawLine(pen1, rigid, lineP2);
                        g.DrawLine(pen1, lineP2, lineP3);
                        g.DrawLine(pen1, lineP3, lineP4);
                        g.DrawLine(pen, lineP4, anim.Location);
                    }
                }
                else
                {
                    lineP2.X = rigid.X - 30;
                    lineP2.Y = rigid.Y;
                    lineP3.X = lineP2.X;
                    if (anim.Y > rigid.Y)
                        lineP3.Y = rigid.Y + 50;
                    else
                        lineP3.Y = rigid.Y - 50;
                    lineP4.X = anim.X - 20;
                    lineP4.Y = lineP3.Y;
                    lineP5.X = lineP4.X;
                    lineP5.Y = anim.Y;
                    g.DrawLine(pen1, rigid, lineP2);
                    g.DrawLine(pen1, lineP2, lineP3);
                    g.DrawLine(pen1, lineP3, lineP4);
                    g.DrawLine(pen1, lineP4, lineP5);
                    g.DrawLine(pen, lineP5, anim.Location);
                }
            }
        }

        public void movePoint1(Point rigid, Point anim, Graphics g)
        {
            Point lineP0 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            Pen pen = new Pen(Color.Black, 2);
            Pen pen1 = new Pen(Color.Black, 2);
            pen.EndCap = LineCap.Custom;
            pen.CustomEndCap = new AdjustableArrowCap(5f, 5f, true);
            //Console.WriteLine(this.EndPoint);
            //Console.WriteLine(anim.X);
            if (Math.Abs(anim.X - rigid.X) < 3 && (rigid.Y - anim.Y) > 20)
            {
                this.Point2 = anim;
                this.Point3 = lineP0;
                this.Point4 = lineP0;
                this.Point5 = lineP0;
                this.Point6 = lineP0;

            }
            //向上折线样式
            else if (Math.Abs(anim.X - rigid.X) < (rigid.Y - anim.Y) && (rigid.Y - anim.Y) > 20)
            {
                lineP2.X = rigid.X;
                lineP2.Y = rigid.Y - (rigid.Y - anim.Y) / 2;
                lineP3.X = anim.X;
                lineP3.Y = lineP2.Y;
                this.point2 = lineP2;
                this.Point3 = lineP3;
                this.Point4 = lineP0;
                this.Point5 = lineP0;
                this.Point6 = lineP0;
            }
            //向上折线样式
            else if ((Math.Abs(anim.X - rigid.X) < (rigid.Y - anim.Y) && (rigid.Y - anim.Y) > 20))
            {
                lineP2.X = rigid.X;
                lineP2.Y = anim.Y;
                this.point2 = lineP2;
                this.Point3 = lineP0;
                this.Point4 = lineP0;
                this.Point5 = lineP0;
                this.Point6 = lineP0;
            }
            //向下折线样式
            else if (Math.Abs(anim.X - rigid.X) > Math.Abs(rigid.Y - anim.Y) && (anim.Y - rigid.Y) > 20)
            {
                if (anim.X > rigid.X)
                {
                    lineP2.X = rigid.X;
                    lineP2.Y = rigid.Y - 20;
                    lineP3.X = anim.X - 20;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = anim.Y;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
                else if (anim.X < rigid.X)
                {
                    lineP2.X = rigid.X;
                    lineP2.Y = rigid.Y - 20;
                    lineP3.X = anim.X + 20;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = anim.Y;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
            }
            //向下折线样式
            else if (Math.Abs(anim.X - rigid.X) < Math.Abs(rigid.Y - anim.Y) && (anim.Y - rigid.Y) > 20)
            {
                if (Math.Abs(anim.X - rigid.X) > 65)
                {
                    lineP2.X = rigid.X;
                    lineP2.Y = rigid.Y - 20;
                    lineP3.X = anim.X;
                    lineP3.Y = lineP2.Y;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP0;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
                else if (Math.Abs(anim.X - rigid.X) < 65)
                {

                    lineP2.X = rigid.X;
                    lineP2.Y = rigid.Y - 20;
                    if (anim.X > rigid.X)
                    {
                        lineP3.X = (int)(rigid.X + 65);
                    }
                    else if (anim.X < rigid.X)
                    {
                        lineP3.X = (int)(rigid.X - 65);
                    }
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = anim.Y - 20;
                    lineP5.X = anim.X;
                    lineP5.Y = lineP4.Y;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.point5 = lineP5;
                    this.Point6 = lineP0;

                }
            }
        }
        public void movePoint13(Point rigid, Point anim, Graphics g)
        {
            Point lineP0 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            Pen pen = new Pen(Color.Black, 2);
            Pen pen1 = new Pen(Color.Black, 2);
            pen.EndCap = LineCap.Custom;
            pen.CustomEndCap = new AdjustableArrowCap(5f, 5f, true);
            //Console.WriteLine(this.EndPoint);
            //Console.WriteLine(anim.X);
            if (Math.Abs(anim.X - rigid.X) < 3 && (anim.Y - rigid.Y) > 20)
            {
                this.Point2 = rigid;
                this.Point3 = lineP0;
                this.Point4 = lineP0;
                this.Point5 = lineP0;
                this.Point6 = lineP0;

            }
            //向上折线样式
            else if (anim.Y - rigid.Y > 20 && Math.Abs(anim.X - rigid.X) > 3)
            {
                lineP2.X = anim.X;
                lineP2.Y = anim.Y - (anim.Y - rigid.Y) / 2;
                lineP3.X = rigid.X;
                lineP3.Y = lineP2.Y;
                lineP4 = rigid;
                this.point2 = lineP2;
                this.Point3 = lineP3;
                this.Point4 = lineP4;
                this.Point5 = lineP0;
                this.Point6 = lineP0;
            }
            else if (anim.Y - rigid.Y < 20 && anim.X - rigid.X > 90)
            {
                lineP2.X = anim.X;
                lineP2.Y = anim.Y - 20;
                lineP3.X = rigid.X + (anim.X - rigid.X) / 2;
                lineP3.Y = lineP2.Y;
                lineP4.X = lineP3.X;
                lineP4.Y = rigid.Y + 20;
                lineP5.X = rigid.X;
                lineP5.Y = lineP4.Y;
                lineP6 = rigid;
                this.point2 = lineP2;
                this.Point3 = lineP3;
                this.Point4 = lineP4;
                this.Point5 = lineP5;
                this.Point6 = lineP6;
            }
            else if (anim.Y - rigid.Y < 20 && anim.X - rigid.X > 0 && anim.X - rigid.X < 90)
            {
                lineP2.X = anim.X;
                lineP2.Y = anim.Y - 20;
                lineP3.X = rigid.X - 65;
                lineP3.Y = lineP2.Y;
                lineP4.X = lineP3.X;
                lineP4.Y = rigid.Y + 20;
                lineP5.X = rigid.X;
                lineP5.Y = lineP4.Y;
                lineP6 = rigid;
                this.point2 = lineP2;
                this.Point3 = lineP3;
                this.Point4 = lineP4;
                this.Point5 = lineP5;
                this.Point6 = lineP6;
            }
            else if (anim.Y - rigid.Y < 20 && rigid.X - anim.X > 90)
            {
                lineP2.X = anim.X;
                lineP2.Y = anim.Y - 20;
                lineP3.X = anim.X + (rigid.X - anim.X) / 2;
                lineP3.Y = lineP2.Y;
                lineP4.X = lineP3.X;
                lineP4.Y = rigid.Y + 20;
                lineP5.X = rigid.X;
                lineP5.Y = lineP4.Y;
                lineP6 = rigid;
                this.point2 = lineP2;
                this.Point3 = lineP3;
                this.Point4 = lineP4;
                this.Point5 = lineP5;
                this.Point6 = lineP6;

            }
            else if (anim.Y - rigid.Y < 20 && rigid.X - anim.X > 0 && rigid.X - anim.X < 90)
            {
                lineP2.X = anim.X;
                lineP2.Y = anim.Y - 20;
                lineP3.X = rigid.X + 65;
                lineP3.Y = lineP2.Y;
                lineP4.X = lineP3.X;
                lineP4.Y = rigid.Y + 20;
                lineP5.X = rigid.X;
                lineP5.Y = lineP4.Y;
                lineP6 = rigid;
                this.point2 = lineP2;
                this.Point3 = lineP3;
                this.Point4 = lineP4;
                this.Point5 = lineP5;
                this.Point6 = lineP6;
            }
            //向下折线样式
            /*else if (Math.Abs(anim.X - rigid.X) > Math.Abs(rigid.Y - anim.Y) && (anim.Y - rigid.Y) > 20)
            {
                if (anim.X > rigid.X)
                {
                    lineP2.X = rigid.X;
                    lineP2.Y = rigid.Y - 20;
                    lineP3.X = anim.X - 20;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = anim.Y;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
                else if (anim.X < rigid.X)
                {
                    lineP2.X = rigid.X;
                    lineP2.Y = rigid.Y - 20;
                    lineP3.X = anim.X + 20;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = anim.Y;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
            }
            //向下折线样式
            else if (Math.Abs(anim.X - rigid.X) < Math.Abs(rigid.Y - anim.Y) && (anim.Y - rigid.Y) > 20)
            {
                if (Math.Abs(anim.X - rigid.X) > 65)
                {
                    lineP2.X = rigid.X;
                    lineP2.Y = rigid.Y - 20;
                    lineP3.X = anim.X;
                    lineP3.Y = lineP2.Y;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP0;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
                else if (Math.Abs(anim.X - rigid.X) < 65)
                {

                    lineP2.X = rigid.X;
                    lineP2.Y = rigid.Y - 20;
                    if (anim.X > rigid.X)
                    {
                        lineP3.X = (int)(rigid.X + 65);
                    }
                    else if (anim.X < rigid.X)
                    {
                        lineP3.X = (int)(rigid.X - 65);
                    }
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = anim.Y - 20;
                    lineP5.X = anim.X;
                    lineP5.Y = lineP4.Y;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.point5 = lineP5;
                    this.Point6 = lineP0;

                }
            }*/
        }
        public void movePoint14(Point rigid, Point anim, Graphics g)
        {
            Point lineP0 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            Pen pen = new Pen(Color.Black, 2);
            Pen pen1 = new Pen(Color.Black, 2);
            pen.EndCap = LineCap.Custom;
            pen.CustomEndCap = new AdjustableArrowCap(5f, 5f, true);
            if (anim.Y > rigid.Y)
            {
                if (rigid.X - anim.X > 20)
                {
                    lineP2.X = anim.X;
                    lineP2.Y = rigid.Y;
                    lineP3 = rigid;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP0;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
                else if (rigid.X - anim.X < 20)
                {
                    lineP2.X = anim.X;
                    lineP2.Y = anim.Y - 20;
                    lineP3.X = rigid.X - 20;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = rigid.Y;
                    lineP5 = rigid;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
            }
            else if (anim.Y < rigid.Y)
            {
                lineP2.X = anim.X;
                lineP2.Y = anim.Y - 20;
                lineP3.X = rigid.X - 20;
                lineP3.Y = lineP2.Y;
                lineP4.X = lineP3.X;
                lineP4.Y = rigid.Y;
                lineP5 = rigid;
                this.point2 = lineP2;
                this.Point3 = lineP3;
                this.Point4 = lineP4;
                this.Point5 = lineP5;
                this.Point6 = lineP0;
            }
        }
        public void movePoint12(Point rigid, Point anim, Graphics g)
        {
            Point lineP0 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            Pen pen = new Pen(Color.Black, 2);
            Pen pen1 = new Pen(Color.Black, 2);
            pen.EndCap = LineCap.Custom;
            pen.CustomEndCap = new AdjustableArrowCap(5f, 5f, true);
            if (anim.Y > rigid.Y)
            {
                if ((anim.X - rigid.X) > 20)
                {
                    lineP2.X = anim.X;
                    lineP2.Y = rigid.Y;
                    lineP3 = rigid;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP0;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
                else if ((anim.X - rigid.X) < 20)
                {
                    lineP2.X = anim.X;
                    lineP2.Y = anim.Y - 20;
                    lineP3.X = rigid.X + 20;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = rigid.Y;
                    lineP5 = rigid;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
            }
            else if (anim.Y < rigid.Y)
            {
                lineP2.X = anim.X;
                lineP2.Y = anim.Y - 20;
                lineP3.X = rigid.X + 20;
                lineP3.Y = lineP2.Y;
                lineP4.X = lineP3.X;
                lineP4.Y = rigid.Y;
                lineP5 = rigid;
                this.point2 = lineP2;
                this.Point3 = lineP3;
                this.Point4 = lineP4;
                this.Point5 = lineP5;
                this.Point6 = lineP0;
            }
        }
        public void movePoint11(Point rigid, Point anim, Graphics g)
        {
            Point lineP0 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            Pen pen = new Pen(Color.Black, 2);
            Pen pen1 = new Pen(Color.Black, 2);
            pen.EndCap = LineCap.Custom;
            pen.CustomEndCap = new AdjustableArrowCap(5f, 5f, true);
            if (anim.Y > rigid.Y)
            {
                if (Math.Abs(anim.X - rigid.X) > 90)
                {
                    lineP2.X = anim.X;
                    lineP2.Y = rigid.Y - 20;
                    lineP3.X = rigid.X;
                    lineP3.Y = lineP2.Y;
                    lineP4 = rigid;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
            }
            if (anim.Y < rigid.Y)
            {
                if (Math.Abs(anim.X - rigid.X) > 90)
                {
                    lineP2.X = anim.X;
                    lineP2.Y = anim.Y - 20;
                    lineP3.X = rigid.X;
                    lineP3.Y = lineP2.Y;
                    lineP4 = rigid;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
            }
        }
        public void movePoint21(Point rigid, Point anim, Graphics g)
        {
            Point lineP0 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            if (anim.Y < rigid.Y)
            {
                if (anim.X < rigid.X)
                {
                    lineP2.X = rigid.X;
                    lineP2.Y = anim.Y;
                    lineP3 = rigid;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP0;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
                else
                {
                    lineP2.X = anim.X + 20;
                    lineP2.Y = anim.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = rigid.Y - 20;
                    lineP4.X = rigid.X;
                    lineP4.Y = lineP3.Y;
                    lineP5 = rigid;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
            }
            else
            {
                if (Math.Abs(rigid.X - anim.X) < 55)
                {
                    lineP2.X = rigid.X + 80;
                    lineP2.Y = anim.Y;
                    lineP3.X = rigid.X + 80;
                    lineP3.Y = rigid.Y - 20;
                    lineP4.X = rigid.X;
                    lineP4.Y = lineP3.Y;
                    lineP5 = rigid;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
                else
                {
                    lineP2.X = anim.X + 20;
                    lineP2.Y = anim.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = rigid.Y - 20;
                    lineP4.X = rigid.X;
                    lineP4.Y = lineP3.Y;
                    lineP5 = rigid;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
            }
        }
        public void movePoint41(Point rigid, Point anim, Graphics g)
        {
            Point lineP0 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            if (anim.Y < rigid.Y)
            {
                if (anim.X > rigid.X)
                {
                    lineP2.X = rigid.X;
                    lineP2.Y = anim.Y;
                    lineP3 = rigid;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP0;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
                else
                {
                    lineP2.X = anim.X - 20;
                    lineP2.Y = anim.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = rigid.Y - 20;
                    lineP4.X = rigid.X;
                    lineP4.Y = lineP3.Y;
                    lineP5 = rigid;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
            }
            else
            {
                if (Math.Abs(rigid.X - anim.X) < 55)
                {
                    lineP2.X = rigid.X - 55;
                    lineP2.Y = anim.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = rigid.Y - 20;
                    lineP4.X = rigid.X;
                    lineP4.Y = lineP3.Y;
                    lineP5 = rigid;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
                else
                {
                    lineP2.X = anim.X - 20;
                    lineP2.Y = anim.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = rigid.Y - 20;
                    lineP4.X = rigid.X;
                    lineP4.Y = lineP3.Y;
                    lineP5 = rigid;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
            }

        }
        public void movePoint1_1(Point rigid, Point anim, Graphics g)
        {
            Point lineP0 = new Point(0, 0), lineP1 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            Pen pen = new Pen(Color.Black, 2);
            Pen pen1 = new Pen(Color.Black, 2);
            pen.EndCap = LineCap.Custom;
            pen.CustomEndCap = new AdjustableArrowCap(5f, 5f, true);
            if (anim.Y > rigid.Y)
            {
                if (Math.Abs(anim.X - rigid.X) > 90)
                {
                    lineP1 = rigid;
                    lineP2.X = rigid.X;
                    lineP2.Y = rigid.Y - 20;
                    lineP3.X = anim.X;
                    lineP3.Y = lineP2.Y;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
            }
            if (anim.Y < rigid.Y)
            {
                if (Math.Abs(anim.X - rigid.X) > 90)
                {
                    lineP1 = rigid;
                    lineP2.X = rigid.X;
                    lineP2.Y = anim.Y - 20;
                    lineP3.X = anim.X;
                    lineP3.Y = lineP2.Y;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    //this.Point4 = lineP4;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
            }
        }

        public Point GetEnd()
        {
            if (!this.point6.IsEmpty)
                return this.point6;
            else if (!this.point5.IsEmpty)
                return this.point5;
            else if (!this.point4.IsEmpty)
                return this.point4;
            else if (!this.point3.IsEmpty)
                return this.point3;
            else return this.point2;
        }
        //第一个点
        public void draw11(BaseGrapthic b1, BaseGrapthic b2)
        {
            Point lineP0 = new Point(0, 0), lineP1 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            if (b1.Point1.Y > b2.Point1.Y)
            {
                lineP1 = b1.Point1;
                lineP2.X = b1.Point1.X;
                lineP2.Y = b2.Point1.Y - 20;
                lineP3.X = b2.Point1.X;
                lineP3.Y = lineP2.Y;
                lineP4 = b2.Point1;
                this.Point1 = lineP1;
                this.point2 = lineP2;
                this.Point3 = lineP3;
                this.Point4 = lineP4;
                this.Point5 = lineP0;
                this.Point6 = lineP0;
            }
            else if (b1.Point1.Y < b2.Point1.Y)
            {
                lineP1 = b1.Point1;
                lineP2.X = b1.Point1.X;
                lineP2.Y = b1.Point1.Y - 20;
                lineP3.X = b2.Point1.X;
                lineP3.Y = lineP2.Y;
                lineP4 = b2.Point1;
                this.Point1 = lineP1;
                this.point2 = lineP2;
                this.Point3 = lineP3;
                this.Point4 = lineP4;
                this.Point5 = lineP0;
                this.Point6 = lineP0;
            }
        }
        public void draw12(BaseGrapthic b1, BaseGrapthic b2)
        {
            Point lineP0 = new Point(0, 0), lineP1 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            if (b1.Point1.Y > b2.Point2.Y + 30)
            {
                //区域1
                if (b1.Point1.X > b2.Point2.X)
                {
                    lineP1 = b1.Point1;
                    lineP2.X = b1.Point1.X;
                    lineP2.Y = b2.Point2.Y;
                    lineP3 = b2.Point2;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP0;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
                //区域2
                else if (b1.Point1.X < b2.Point2.X)
                {
                    lineP1 = b1.Point1;
                    lineP2.X = b1.Point1.X;
                    lineP2.Y = b1.Point1.Y - 20;
                    lineP3.X = b2.Point2.X + 20;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = b2.Point2.Y;
                    lineP5 = b2.Point2;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
            }
            else if (b1.Point1.Y < b2.Point2.Y + 30)
            {
                //区域三
                if (b1.Point1.X - b2.Point2.X > 65)
                {
                    lineP1 = b1.Point1;
                    lineP2.X = b1.Point1.X;
                    lineP2.Y = b1.Point1.Y - 20;
                    lineP3.X = b2.Point2.X + 20;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = b2.Point2.Y;
                    lineP5 = b2.Point2;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
                else if (b1.Point1.X - b2.Point2.X > 0)
                {
                    lineP1 = b1.Point1;
                    lineP2.X = b1.Point1.X;
                    lineP2.Y = b1.Point1.Y - 20;
                    lineP3.X = b1.Point1.X + 65;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = b2.Point2.Y;
                    lineP5 = b2.Point2;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
                //区域4
                else if (b1.Point1.X < b2.Point2.X)
                {
                    lineP1 = b1.Point1;
                    lineP2.X = b1.Point1.X;
                    lineP2.Y = b1.Point1.Y - 20;
                    lineP3.X = b2.Point2.X + 20;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = b2.Point2.Y;
                    lineP5 = b2.Point2;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
            }
        }
        public void draw13(BaseGrapthic b1, BaseGrapthic b2)
        {
            Point lineP0 = new Point(0, 0), lineP1 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            if (b1.Point1.Y > b2.Point3.Y)
            {
                lineP1 = b1.Point1;
                lineP2.X = b1.Point1.X;
                lineP2.Y = b2.Point3.Y + (b1.Point1.Y - b2.Point3.Y) / 2;
                lineP3.X = b2.Point3.X;
                lineP3.Y = lineP2.Y;
                lineP4 = b2.Point3;
                this.Point1 = lineP1;
                this.point2 = lineP2;
                this.Point3 = lineP3;
                this.Point4 = lineP4;
                this.Point5 = lineP0;
                this.Point6 = lineP0;
            }
            else if (b1.Point1.Y < b2.Point3.Y)
            {
                if (Math.Abs(b1.Point1.X - b2.Point3.X) > 110)
                {
                    lineP1 = b1.Point1;
                    lineP2.X = b1.Point1.X;
                    lineP2.Y = b1.Point1.Y - 20;
                    if (b1.Point1.X > b2.Point3.X)
                        lineP3.X = b1.Point1.X - Math.Abs(b1.Point1.X - b2.Point3.X) / 2;
                    else if (b1.Point1.X < b2.Point3.X)
                        lineP3.X = b1.Point1.X + Math.Abs(b1.Point1.X - b2.Point3.X) / 2;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = b2.Point3.Y + 20;
                    lineP5.X = b2.Point3.X;
                    lineP5.Y = lineP4.Y;
                    lineP6 = b2.Point3;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP6;
                }
                else if (Math.Abs(b1.Point1.X - b2.Point3.X) > 0)
                {
                    lineP1 = b1.Point1;
                    lineP2.X = b1.Point1.X;
                    lineP2.Y = b1.Point1.Y - 20;
                    if (b1.Point1.X > b2.Point3.X)
                        lineP3.X = b2.Point3.X - 65;
                    else if (b1.Point1.X < b2.Point3.X)
                        lineP3.X = b2.Point3.X + 65;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = b2.Point3.Y + 20;
                    lineP5.X = b2.Point3.X;
                    lineP5.Y = lineP4.Y;
                    lineP6 = b2.Point3;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP6;
                }
            }
        }
        public void draw14(BaseGrapthic b1, BaseGrapthic b2)
        {
            Point lineP0 = new Point(0, 0), lineP1 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            if (b1.Point1.Y > b2.Point2.Y + 30)
            {
                if (b1.Point1.X > b2.Point4.X)
                {
                    lineP1 = b1.Point1;
                    lineP2.X = b1.Point1.X;
                    lineP2.Y = b1.Point1.Y - 20;
                    lineP3.X = b2.Point4.X - 20;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = b2.Point4.Y;
                    lineP5 = b2.Point4;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
                else if (b1.Point1.X < b2.Point4.X)
                {
                    lineP1 = b1.Point1;
                    lineP2.X = b1.Point1.X;
                    lineP2.Y = b2.Point4.Y;
                    lineP3 = b2.Point4;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP0;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
            }
            else if (b1.Point1.Y < b2.Point2.Y + 30)
            {
                if (b1.Point1.X - b2.Point1.X > 0)
                {
                    lineP1 = b1.Point1;
                    lineP2.X = b1.Point1.X;
                    lineP2.Y = b1.Point1.Y - 20;
                    lineP3.X = b2.Point4.X - 20;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = b2.Point4.Y;
                    lineP5 = b2.Point4;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
                else if (b1.Point1.X - b2.Point1.X > -110)
                {
                    lineP1 = b1.Point1;
                    lineP2.X = b1.Point1.X;
                    lineP2.Y = b1.Point1.Y - 20;
                    lineP3.X = b1.Point1.X - 85;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = b2.Point4.Y;
                    lineP5 = b2.Point4;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
                else if (b1.Point1.X - b2.Point1.X < -110)
                {
                    lineP1 = b1.Point1;
                    lineP2.X = b1.Point1.X;
                    lineP2.Y = b1.Point1.Y - 20;
                    lineP3.X = b2.Point4.X - 20;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = b2.Point4.Y;
                    lineP5 = b2.Point4;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
            }
        }
        public void draw21(BaseGrapthic b1, BaseGrapthic b2)
        {
            Point lineP0 = new Point(0, 0), lineP1 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            if (b1.Point2.X < b2.Point4.X - 20)
            {
                if (b1.Point2.Y < b2.Point1.Y)
                {
                    lineP1 = b1.Point2;
                    lineP2.X = b2.Point1.X;
                    lineP2.Y = b1.Point2.Y;
                    lineP3 = b2.Point1;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP0;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
                else
                {
                    lineP1 = b1.Point2;
                    lineP2.X = lineP1.X + 20;
                    lineP2.Y = lineP1.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = b2.Point1.Y - 20;
                    lineP4.X = b2.Point1.X;
                    lineP4.Y = lineP3.Y;
                    lineP5 = b2.Point1;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
            }
            else
            {
                if (b1.Point4.Y < b2.Point1.Y || b1.Point1.Y > b2.Point1.Y)
                {
                    lineP1 = b1.Point2;
                    lineP2.X = lineP1.X + 20;
                    lineP2.Y = lineP1.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = b2.Point1.Y - 20;
                    lineP4.X = b2.Point1.X;
                    lineP4.Y = lineP3.Y;
                    lineP5 = b2.Point1;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
                if (b2.Point1.Y - b1.Point1.Y > 20 && b2.Point1.Y - b1.Point1.Y < 80)
                {
                    lineP1 = b1.Point2;
                    lineP2.X = lineP1.X + 20;
                    lineP2.Y = lineP1.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = b1.Point1.Y - 20;
                    lineP4.X = b2.Point1.X;
                    lineP4.Y = lineP3.Y;
                    lineP5 = b2.Point1;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
            }
        }
        public void draw22(BaseGrapthic b1, BaseGrapthic b2)
        {
            Point lineP0 = new Point(0, 0), lineP1 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            if (b1.Point2.X > b2.Point2.X)
            {
                lineP1 = b1.Point2;
                lineP2.X = b1.Point2.X + 20;
                lineP2.Y = b1.Point2.Y;
                lineP3.X = lineP2.X;
                lineP3.Y = b2.Point2.Y;
                lineP4 = b2.Point2;
                this.Point1 = lineP1;
                this.point2 = lineP2;
                this.Point3 = lineP3;
                this.Point4 = lineP4;
                this.Point5 = lineP0;
                this.Point6 = lineP0;
            }
            else
            {
                lineP1 = b1.Point2;
                lineP2.X = b2.Point2.X + 20;
                lineP2.Y = b1.Point2.Y;
                lineP3.X = lineP2.X;
                lineP3.Y = b2.Point2.Y;
                lineP4 = b2.Point2;
                this.Point1 = lineP1;
                this.point2 = lineP2;
                this.Point3 = lineP3;
                this.Point4 = lineP4;
                this.Point5 = lineP0;
                this.Point6 = lineP0;
            }
        }
        public void draw23(BaseGrapthic b1, BaseGrapthic b2)
        {
            Point lineP0 = new Point(0, 0), lineP1 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            if (b1.Point2.Y > b2.Point3.Y && b1.Point2.X < b2.Point3.X)
            {
                lineP1 = b1.Point2;
                lineP2.X = b2.Point3.X;
                lineP2.Y = lineP1.Y;
                lineP3 = b2.Point3;
                this.Point1 = lineP1;
                this.point2 = lineP2;
                this.Point3 = lineP3;
                this.Point4 = lineP0;
                this.Point5 = lineP0;
                this.Point6 = lineP0;
            }
            else if (b1.Point2.Y < b2.Point3.Y && b2.Point4.X < b1.Point2.X && b2.Point4.X > b1.Point4.X)
            {
                lineP1 = b1.Point2;
                lineP2.X = b2.Point3.X + 65;
                lineP2.Y = lineP1.Y;
                lineP3.X = lineP2.X;
                lineP3.Y = b2.Point3.Y + 20;
                lineP4.X = b2.Point3.X;
                lineP4.Y = lineP3.Y;
                lineP5 = b2.Point3;
                this.Point1 = lineP1;
                this.point2 = lineP2;
                this.Point3 = lineP3;
                this.Point4 = lineP4;
                this.Point5 = lineP5;
                this.Point6 = lineP0;
            }
            else
            {

                lineP1 = b1.Point2;
                lineP2.X = lineP1.X + 20;
                lineP2.Y = lineP1.Y;
                lineP3.X = lineP2.X;
                lineP3.Y = b2.Point3.Y + 20;
                lineP4.X = b2.Point3.X;
                lineP4.Y = lineP3.Y;
                lineP5 = b2.Point3;
                this.Point1 = lineP1;
                this.point2 = lineP2;
                this.Point3 = lineP3;
                this.Point4 = lineP4;
                this.Point5 = lineP5;
                this.Point6 = lineP0;

            }
        }
        public void draw24(BaseGrapthic b1, BaseGrapthic b2)
        {
            Point lineP0 = new Point(0, 0), lineP1 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            if (b1.Point2.X < b2.Point4.X)
            {
                if (b1.Point2.Y == b2.Point4.Y)
                {
                    lineP1 = b1.Point2;
                    lineP2 = b2.Point4;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP0;
                    this.Point4 = lineP0;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
                else
                {

                    lineP1 = b1.Point2;
                    lineP2.X = b1.Point2.X + (b2.Point4.X - b1.Point2.X) / 2;
                    lineP2.Y = b1.Point2.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = b2.Point4.Y;
                    lineP4 = b2.Point4;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }

            }
            else
            {
                if (b2.Point3.Y > b1.Point1.Y && b2.Point3.Y < b1.Point3.Y)
                {
                    lineP1 = b1.Point2;
                    lineP2.X = lineP1.X + 20;
                    lineP2.Y = lineP1.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = b2.Point4.Y - 50;
                    lineP4.X = b2.Point4.X - 20;
                    lineP4.Y = lineP3.Y;
                    lineP5.X = lineP4.X;
                    lineP5.Y = b2.Point4.Y;
                    lineP6 = b2.Point4;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP6;
                }
                else if (b2.Point1.Y > b1.Point1.Y && b2.Point1.Y < b1.Point3.Y)
                {
                    lineP1 = b1.Point2;
                    lineP2.X = lineP1.X + 20;
                    lineP2.Y = lineP1.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = b2.Point4.Y + 50;
                    lineP4.X = b2.Point4.X - 20;
                    lineP4.Y = lineP3.Y;
                    lineP5.X = lineP4.X;
                    lineP5.Y = b2.Point4.Y;
                    lineP6 = b2.Point4;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP6;
                }
                else
                {
                    lineP1 = b1.Point2;
                    lineP2.X = lineP1.X + 20;
                    lineP2.Y = lineP1.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = lineP2.Y + (b2.Point4.Y - b1.Point2.Y) / 2;
                    lineP4.X = b2.Point4.X - 20;
                    lineP4.Y = lineP3.Y;
                    lineP5.X = lineP4.X;
                    lineP5.Y = b2.Point4.Y;
                    lineP6 = b2.Point4;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP6;
                }
            }
        }
        public void draw31(BaseGrapthic b1, BaseGrapthic b2)
        {
            Point lineP0 = new Point(0, 0), lineP1 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            if (b1.Point3.Y < b2.Point1.Y)
            {
                if (b1.Point3.X == b2.Point1.X)
                {
                    lineP1 = b1.Point3;
                    lineP2 = b2.Point1;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP0;
                    this.Point4 = lineP0;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
                else
                {
                    lineP1 = b1.Point3;
                    lineP2.X = lineP1.X;
                    lineP2.Y = b1.Point3.Y + (b2.Point1.Y - b1.Point3.Y) / 2;
                    lineP3.X = b2.Point1.X;
                    lineP3.Y = lineP2.Y;
                    lineP4 = b2.Point1;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
            }
            else
            {
                if (Math.Abs(b1.Point3.X - b2.Point1.X) > 110)
                {

                    lineP1 = b1.Point3;
                    lineP2.X = lineP1.X;
                    lineP2.Y = lineP1.Y + 20;
                    if (b1.Point3.X < b2.Point1.X)
                        lineP3.X = lineP2.X + Math.Abs(b1.Point3.X - b2.Point1.X) / 2;
                    else
                        lineP3.X = lineP2.X - Math.Abs(b1.Point3.X - b2.Point1.X) / 2;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = b2.Point1.Y - 20;
                    lineP5.X = b2.Point1.X;
                    lineP5.Y = lineP4.Y;
                    lineP6 = b2.Point1;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP6;
                }
                else
                {
                    lineP1 = b1.Point3;
                    lineP2.X = lineP1.X;
                    lineP2.Y = lineP1.Y + 20;
                    if (b1.Point3.X < b2.Point1.X)
                        lineP3.X = b1.Point3.X - 65;
                    else
                        lineP3.X = b1.Point3.X + 65;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = b2.Point1.Y - 20;
                    lineP5.X = b2.Point1.X;
                    lineP5.Y = lineP4.Y;
                    lineP6 = b2.Point1;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP6;
                }
            }
        }
        public void draw32(BaseGrapthic b1, BaseGrapthic b2)
        {
            Point lineP0 = new Point(0, 0), lineP1 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            if (b1.Point3.Y < b2.Point2.Y)
            {
                if (b1.Point3.X > b2.Point2.X)
                {
                    lineP1 = b1.Point3;
                    lineP2.X = lineP1.X;
                    lineP2.Y = b2.Point2.Y;
                    lineP3 = b2.Point2;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP0;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
                else
                {
                    lineP1 = b1.Point3;
                    lineP2.X = lineP1.X;
                    lineP2.Y = lineP1.Y + 20;
                    lineP3.X = b2.Point2.X + 20;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = b2.Point2.Y;
                    lineP5 = b2.Point2;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
            }
            else
            {
                if (b1.Point3.X - 45 > b2.Point2.X || b2.Point2.X - 25 > b1.Point3.X)
                {
                    lineP1 = b1.Point3;
                    lineP2.X = lineP1.X;
                    lineP2.Y = lineP1.Y + 20;
                    lineP3.X = b2.Point2.X + 20;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = b2.Point2.Y;
                    lineP5 = b2.Point2;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
                else
                {
                    lineP1 = b1.Point3;
                    lineP2.X = lineP1.X;
                    lineP2.Y = lineP1.Y + 20;
                    lineP3.X = b1.Point3.X + 65;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = b2.Point2.Y;
                    lineP5 = b2.Point2;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
            }
        }
        public void draw33(BaseGrapthic b1, BaseGrapthic b2)
        {
            Point lineP0 = new Point(0, 0), lineP1 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            if (b1.Point3.Y > b2.Point3.Y)
            {
                lineP1 = b1.Point3;
                lineP2.X = lineP1.X;
                lineP2.Y = lineP1.Y + 20;
                lineP3.X = b2.Point3.X;
                lineP3.Y = lineP2.Y;
                lineP4 = b2.Point3;
                this.Point1 = lineP1;
                this.point2 = lineP2;
                this.Point3 = lineP3;
                this.Point4 = lineP4;
                this.Point5 = lineP0;
                this.Point6 = lineP0;
            }
            else
            {
                lineP1 = b1.Point3;
                lineP2.X = lineP1.X;
                lineP2.Y = b2.Point3.Y + 20;
                lineP3.X = b2.Point3.X;
                lineP3.Y = lineP2.Y;
                lineP4 = b2.Point3;
                this.Point1 = lineP1;
                this.point2 = lineP2;
                this.Point3 = lineP3;
                this.Point4 = lineP4;
                this.Point5 = lineP0;
                this.Point6 = lineP0;
            }
        }
        public void draw34(BaseGrapthic b1, BaseGrapthic b2)
        {
            Point lineP0 = new Point(0, 0), lineP1 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            if (b1.Point3.Y < b2.Point4.Y)
            {
                if (b1.Point3.X < b2.Point4.X)
                {
                    lineP1 = b1.Point3;
                    lineP2.X = lineP1.X;
                    lineP2.Y = b2.Point4.Y;
                    lineP3= b2.Point4;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP0;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
                else
                {
                    lineP1 = b1.Point3;
                    lineP2.X = lineP1.X;
                    lineP2.Y = lineP1.Y+20;
                    lineP3.X= b2.Point4.X-20;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = b2.Point4.Y;
                    lineP5 = b2.Point4;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
            }
            else
            {
                if (b2.Point4.X > b1.Point4.X && b2.Point4.X < b1.Point2.X)
                {
                    lineP1 = b1.Point3;
                    lineP2.X = lineP1.X;
                    lineP2.Y = lineP1.Y + 20;
                    lineP3.X =b1.Point3.X-65;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = b2.Point4.Y;
                    lineP5 = b2.Point4;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
                else
                {
                    lineP1 = b1.Point3;
                    lineP2.X = lineP1.X;
                    lineP2.Y = lineP1.Y + 20;
                    lineP3.X = b2.Point4.X - 20;
                    lineP3.Y = lineP2.Y;
                    lineP4.X = lineP3.X;
                    lineP4.Y = b2.Point4.Y;
                    lineP5 = b2.Point4;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;

                }
            }
        }
        public void draw41(BaseGrapthic b1, BaseGrapthic b2)
        {
            Point lineP0 = new Point(0, 0), lineP1 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            if (b1.Point4.X - 20 > b2.Point2.X)
            {
                if (b1.Point4.Y < b2.Point1.Y)
                {
                    lineP1 = b1.Point4;
                    lineP2.X = b2.Point1.X;
                    lineP2.Y = b1.Point4.Y;
                    lineP3 = b2.Point1;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP0;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
                else
                {
                    lineP1 = b1.Point4;
                    lineP2.X = lineP1.X - 20;
                    lineP2.Y = lineP1.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = b2.Point1.Y - 20;
                    lineP4.X = b2.Point1.X;
                    lineP4.Y = lineP3.Y;
                    lineP5 = b2.Point1;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
            }
            else
            {
                if (b1.Point4.Y < b2.Point1.Y || b1.Point1.Y > b2.Point1.Y)
                {
                    lineP1 = b1.Point4;
                    lineP2.X = lineP1.X - 20;
                    lineP2.Y = lineP1.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = b2.Point1.Y - 20;
                    lineP4.X = b2.Point1.X;
                    lineP4.Y = lineP3.Y;
                    lineP5 = b2.Point1;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
                if (b2.Point1.Y - b1.Point1.Y > 20 && b2.Point1.Y - b1.Point1.Y < 80)
                {
                    lineP1 = b1.Point4;
                    lineP2.X = lineP1.X - 20;
                    lineP2.Y = lineP1.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = b1.Point1.Y - 20;
                    lineP4.X = b2.Point1.X;
                    lineP4.Y = lineP3.Y;
                    lineP5 = b2.Point1;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
            }
        }
        public void draw42(BaseGrapthic b1, BaseGrapthic b2)
        {
            Point lineP0 = new Point(0, 0), lineP1 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            if (b1.Point4.X > b2.Point2.X)
            {
                if(b1.Point4.Y == b2.Point2.Y)
                {
                    lineP1 = b1.Point4;
                    lineP2 = b2.Point2;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP0;
                    this.Point4 = lineP0;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
                else
                {
                    lineP1 = b1.Point4;
                    lineP2.X= b2.Point2.X+(b1.Point4.X-b2.Point2.X)/2;
                    lineP2.Y = b1.Point4.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = b2.Point2.Y;
                    lineP4 = b2.Point2;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
            }
            else
            {
                if (b2.Point3.Y < b1.Point3.Y && b2.Point3.Y > b1.Point1.Y)
                {
                    lineP1 = b1.Point4;
                    lineP2.X = lineP1.X - 20 ;
                    lineP2.Y = lineP1.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = lineP2.Y + 50;
                    lineP4.X = b2.Point2.X + 20;
                    lineP4.Y = lineP3.Y;
                    lineP5.X = lineP4.X;
                    lineP5.Y = b2.Point2.Y;
                    lineP6 = b2.Point2;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP6;
                }
                else
                {
                    lineP1 = b1.Point4;
                    lineP2.X = lineP1.X - 20;
                    lineP2.Y = lineP1.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = lineP2.Y + (b2.Point2.Y - b1.Point4.Y) / 2;
                    lineP4.X = b2.Point2.X + 20;
                    lineP4.Y = lineP3.Y;
                    lineP5.X = lineP4.X;
                    lineP5.Y = b2.Point2.Y;
                    lineP6 = b2.Point2;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP6;
                }
            }
        }
        public void draw43(BaseGrapthic b1, BaseGrapthic b2)
        {
            Point lineP0 = new Point(0, 0), lineP1 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            if (b1.Point4.X > b2.Point3.X)
            {
                if (b1.Point4.Y < b2.Point3.Y)
                {
                    lineP1 = b1.Point4;
                    lineP2.X = b1.Point4.X-20;
                    lineP2.Y = b1.Point4.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = b2.Point3.Y + 20;
                    lineP4.X = b2.Point3.X;
                    lineP4.Y = lineP3.Y;
                    lineP5 = b2.Point3;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
                else
                {
                    lineP1 = b1.Point4;
                    lineP2.X = b2.Point3.X;
                    lineP2.Y = b1.Point4.Y;
                    lineP3 = b2.Point3;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP0;
                    this.Point5 = lineP0;
                    this.Point6 = lineP0;
                }
            }
            else
            {
                if (b2.Point3.Y > b1.Point1.Y && b2.Point3.Y < b1.Point3.Y)
                {
                    lineP1 = b1.Point4;
                    lineP2.X = b1.Point4.X - 20;
                    lineP2.Y = b1.Point4.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = b1.Point4.Y + 50;
                    lineP4.X = b2.Point3.X;
                    lineP4.Y = lineP3.Y;
                    lineP5 = b2.Point3;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
                else
                {
                    lineP1 = b1.Point4;
                    lineP2.X = b1.Point4.X - 20;
                    lineP2.Y = b1.Point4.Y;
                    lineP3.X = lineP2.X;
                    lineP3.Y = b2.Point3.Y + 20;
                    lineP4.X = b2.Point3.X;
                    lineP4.Y = lineP3.Y;
                    lineP5 = b2.Point3;
                    this.Point1 = lineP1;
                    this.point2 = lineP2;
                    this.Point3 = lineP3;
                    this.Point4 = lineP4;
                    this.Point5 = lineP5;
                    this.Point6 = lineP0;
                }
            }
        }
        public void draw44(BaseGrapthic b1, BaseGrapthic b2)
        {
            Point lineP0 = new Point(0, 0), lineP1 = new Point(0, 0), lineP2 = new Point(0, 0), lineP3 = new Point(0, 0), lineP4 = new Point(0, 0), lineP5 = new Point(0, 0), lineP6 = new Point(0, 0);
            if (b1.Point4.X < b2.Point4.X)
            {
                lineP1 = b1.Point4;
                lineP2.X = lineP1.X - 20;
                lineP2.Y = lineP1.Y;
                lineP3.X = lineP2.X;
                lineP3.Y = b2.Point4.Y;
                lineP4 = b2.Point4;
                this.Point1 = lineP1;
                this.point2 = lineP2;
                this.Point3 = lineP3;
                this.Point4 = lineP4;
                this.Point5 = lineP0;
                this.Point6 = lineP0;
            }
            else
            {
                lineP1 = b1.Point4;
                lineP2.X = b2.Point4.X - 20;
                lineP2.Y = lineP1.Y;
                lineP3.X = lineP2.X;
                lineP3.Y = b2.Point4.Y;
                lineP4 = b2.Point4;
                this.Point1 = lineP1;
                this.point2 = lineP2;
                this.Point3 = lineP3;
                this.Point4 = lineP4;
                this.Point5 = lineP0;
                this.Point6 = lineP0;
            }
        }
        public void draw(Graphics g)
        {
            Pen pen = new Pen(Color.Black, 2);
            Pen pen1 = new Pen(Color.Black, 2);
            pen.EndCap = LineCap.Custom;
            pen.CustomEndCap = new AdjustableArrowCap(5f, 5f, true);
            if (this.Point3.IsEmpty && this.Point4.IsEmpty && this.Point5.IsEmpty && this.Point6.IsEmpty)
            {
                g.DrawLine(pen, this.Point1, this.Point2);
            }
            else if (this.Point4.IsEmpty && this.Point5.IsEmpty && this.Point6.IsEmpty)
            {
                g.DrawLine(pen1, this.point1, this.point2);
                g.DrawLine(pen, this.point2, this.point3);
            }
            else if (this.Point5.IsEmpty && this.Point6.IsEmpty)
            {
                g.DrawLine(pen1, this.Point1, this.Point2);
                g.DrawLine(pen1, this.Point2, this.Point3);
                g.DrawLine(pen, this.Point3, this.Point4);
            }
            else if (this.Point6.IsEmpty)
            {
                g.DrawLine(pen1, this.Point1, this.Point2);
                g.DrawLine(pen1, this.Point2, this.Point3);
                g.DrawLine(pen1, this.Point3, this.Point4);
                g.DrawLine(pen, this.Point4, this.Point5);
            }
            else if (!this.point1.IsEmpty && !this.point2.IsEmpty && !this.point3.IsEmpty && !this.point4.IsEmpty && !this.point5.IsEmpty && !this.point6.IsEmpty)
            {
                //Console.WriteLine("draw");
                g.DrawLine(pen1, this.Point1, this.Point2);
                g.DrawLine(pen1, this.Point2, this.Point3);
                g.DrawLine(pen1, this.Point3, this.Point4);
                g.DrawLine(pen1, this.Point4, this.Point5);
                g.DrawLine(pen, this.Point5, this.Point6);
            }
        }
    }
}
