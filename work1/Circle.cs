﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace work1
{
    [Serializable]
    public class Circle : BaseGrapthic
    {
        private float x;
        private float y;
        private float width;
        private float height;
        private bool isSelected;
        private List<Line> lineList;
        private Point point1;
        private Point point2;
        private Point point3;
        private Point point4;
        public Circle()
        {
            this.x = 20;
            this.Y = 60;
            this.Width = 20;
            this.Height = 20;
            this.IsSelected = false;
            this.LineList = new List<Line>();
            this.point1.X = (int)this.X + 10;
            this.point1.Y = (int)this.Y;
            this.point2.X = (int)this.X + 20;
            this.point2.Y = (int)this.Y + 10;
            this.point3.X = (int)this.X + 10;
            this.point3.Y = (int)this.Y + 20;
            this.point4.X = (int)this.X;
            this.point4.Y = (int)this.Y + 10;
        }
        public float X { get => x; set => x = value; }
        public float Y { get => y; set => y = value; }
        public float Width { get => width; set => width = value; }
        public float Height { get => height; set => height = value; }
        public bool IsSelected { get => isSelected; set => isSelected = value; }
        public override Point Point1 { get => point1; set => point1 = value; }
        public override Point Point2 { get => point2; set => point2 = value; }
        public override Point Point3 { get => point3; set => point3 = value; }
        public override Point Point4 { get => point4; set => point4 = value; }
        public override List<Line> LineList { get => lineList; set => lineList = value; }

        public override void Accept(Visitor visitor, Graphics g)
        {
            visitor.Visit(this,g);
        }

        public override void add()
        {
            throw new NotImplementedException();
        }
        public override bool contains(int x, int y)
        {
            float rx = this.X + this.width / 2;
            float ry = this.y + this.Height / 2;
            float d1 = (float)Math.Pow(rx - x, 2);
            float d2 = (float)Math.Pow(ry - y, 2);
            float d = (float)Math.Sqrt(d1 + d2);
            if (d<7)
            {
                return true;
            }
            else return false;
        }

        public override bool contains(Point point)
        {
            float rx = this.X + this.width / 2;
            float ry = this.y + this.Height / 2;
            float d1 = (float)Math.Pow(rx - point.X, 2);
            float d2 = (float)Math.Pow(ry - point.Y, 2);
            float d = (float)Math.Sqrt(d1 + d2);
            if (d < 7)
            {
                return true;
            }
            else return false;
        }

        public override void draw(Graphics g)
        {
            Pen p = new Pen(Color.Black, 2);
            g.DrawEllipse(p, this.x, this.y, this.width, this.height);
            p.Dispose();
            g.Dispose();
        }

        public override void drawCircle(Graphics g)
        {
            float x1 = this.X + (this.Width / 2) - 3;
            float y1 = this.Y - 3;
            float x2 = this.X - 3;
            float y2 = this.Y + (this.Height / 2) - 3;
            float x3 = this.X + this.Width - 3;
            float y3 = this.Y + (this.Height / 2) - 3;
            float x4 = this.X + (this.Width / 2) - 3;
            float y4 = this.Y + this.Height - 3;
            Rectangle rect1 = new Rectangle((int)x1, (int)y1, 6, 6);
            Rectangle rect2 = new Rectangle((int)x2, (int)y2, 6, 6);
            Rectangle rect3 = new Rectangle((int)x3, (int)y3, 6, 6);
            Rectangle rect4 = new Rectangle((int)x4, (int)y4, 6, 6);
            Pen pen = new Pen(Color.Black, 1);
            g.DrawEllipse(pen, rect1);
            g.DrawEllipse(pen, rect2);
            g.DrawEllipse(pen, rect3);
            g.DrawEllipse(pen, rect4);
        }

        public override void fill(Graphics g, Color color)
        {
            g.FillEllipse(new SolidBrush(color), this.X, this.Y, this.Width, this.Height);
        }

        public override int isConnected(int x, int y)
        {
            float x1 = this.X + (this.Width / 2);
            float y1 = this.Y;
            float x2 = this.X;
            float y2 = this.Y + (this.Height / 2);
            float x3 = this.X + this.Width;
            float y3 = this.Y + (this.Height / 2);
            float x4 = this.X + (this.Width / 2);
            float y4 = this.Y + this.Height;
            if (x > x1 - 5 && x < x1 + 5 && y > y1 - 5 && y < y1 + 5)
            {
                return 1;
            }
            else if (x > x2 - 5 && x < x2 + 5 && y > y2 - 5 && y < y2 + 5)
            {
                return 4;
            }
            else if (x > x3 - 5 && x < x3 + 5 && y > y3 - 5 && y < y3 + 5)
            {
                return 2;
            }
            else if (x > x4 - 5 && x < x4 + 5 && y > y4 - 5 && y < y4 + 5)
            {
                return 3;
            }
            else return 0;
        }

        public override int isConnected(Point point)
        {
            float x1 = this.X + (this.Width / 2);
            float y1 = this.Y;
            float x2 = this.X;
            float y2 = this.Y + (this.Height / 2);
            float x3 = this.X + this.Width;
            float y3 = this.Y + (this.Height / 2);
            float x4 = this.X + (this.Width / 2);
            float y4 = this.Y + this.Height;
            if (point.X > x1 - 5 && point.X < x1 + 5 && point.Y > y1 - 5 && point.Y < y1 + 5)
            {
                return 1;
            }
            else if (point.X > x2 - 5 && point.X < x2 + 5 && point.Y > y2 - 5 && point.Y < y2 + 5)
            {
                return 4;
            }
            else if (point.X > x3 - 5 && point.X < x3 + 5 && point.Y > y3 - 5 && point.Y < y3 + 5)
            {
                return 2;
            }
            else if (point.X > x4 - 5 && point.X < x4 + 5 && point.Y > y4 - 5 && point.Y < y4 + 5)
            {
                return 3;
            }
            else return 0;
        }

        public override void remove()
        {
            throw new NotImplementedException();
        }

    }
}
