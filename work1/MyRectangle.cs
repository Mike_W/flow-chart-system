﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace work1
{
    [Serializable]
    public class MyRectangle : BaseGrapthic

    {
        private float x;
        private float y;
        private float width;
        private float height;
        private bool isSelected;
        private List<Line> lineList;
        private int selected;
        private Point point1;
        private Point point2;
        private Point point3;
        private Point point4;
        public MyRectangle()
        {
            this.X = 20;
            this.Y = 60;
            this.Width = 90;
            this.Height = 50;
            this.isSelected = false;
            this.Selected = 0;
            this.lineList = new List<Line>();
            this.point1.X = (int)this.X + 45;
            this.point1.Y = (int)this.Y;
            this.point2.X = (int)this.X + 90;
            this.point2.Y = (int)this.Y + 25;
            this.point3.X = (int)this.X + 45;
            this.point3.Y = (int)this.Y + 50;
            this.point4.X = (int)this.X;
            this.point4.Y = (int)this.Y + 25;
        }

        public float Y { get => y; set => y = value; }
        public float X { get => x; set => x = value; }
        public float Width { get => width; set => width = value; }
        public float Height { get => height; set => height = value; }
        public bool IsSelected { get => isSelected; set => isSelected = value; }
        public override List<Line> LineList { get => lineList; set => lineList = value; }
        public int Selected { get => selected; set => selected = value; }
        public override Point Point1 { get => point1; set => point1 = value; }
        public override Point Point2 { get => point2; set => point2 = value; }
        public override Point Point3 { get => point3; set => point3 = value; }
        public override Point Point4 { get => point4; set => point4 = value; }

        public override void add()
        {
            throw new NotImplementedException();
        }
        public override void draw(Graphics g)
        {
            Pen p = new Pen(Color.Black, 2);
            g.DrawRectangle(p, this.X, this.Y, this.Width, this.Height);
            p.Dispose();
            g.Dispose();
        }
    
        public override void remove()
        {
            throw new NotImplementedException();
        }

        public override bool contains(int x, int y)
        {
            if (x > this.X && x < this.X + this.Width && y > this.Y && y < this.Y + this.Height)
            {
                return true;
            }
            else return false;
        }

        public override bool contains(Point p)
        {
            if (p.X > this.X && p.X < this.X + this.Width && p.Y > this.Y && p.Y < this.X + this.Height)
            {
                return true;
            }
            else return false;
        }

        public override int isConnected(int x, int y)
        {
            float x1 = this.X + (this.Width / 2);
            float y1 = this.Y;
            float x2 = this.X;
            float y2 = this.Y + (this.Height / 2);
            float x3 = this.X + this.Width;
            float y3 = this.Y + (this.Height / 2);
            float x4 = this.X + (this.Width / 2);
            float y4 = this.Y + this.Height;
            if (x > x1 - 5 && x < x1 + 5 && y > y1 - 5 && y < y1 + 5)
            {
                return 1;
            }
            else if (x > x2 - 5 && x < x2 + 5 && y > y2 - 5 && y < y2 + 5)
            {
                return 4;
            }
            else if (x > x3 - 5 && x < x3 + 5 && y > y3 - 5 && y < y3 + 5)
            {
                return 2;
            }
            else if (x > x4 - 5 && x < x4 + 5 && y > y4 - 5 && y < y4 + 5)
            {
                return 3;
            }
            else return 0;
        }

        public override int isConnected(Point point)
        {
            float x1 = this.X + (this.Width / 2);
            float y1 = this.Y;
            float x2 = this.X;
            float y2 = this.Y + (this.Height / 2);
            float x3 = this.X + this.Width;
            float y3 = this.Y + (this.Height / 2);
            float x4 = this.X + (this.Width / 2);
            float y4 = this.Y + this.Height;
            if (point.X > x1 - 5 && point.X < x1 + 5 && point.Y > y1 - 5 && point.Y < y1 + 5)
            {
                return 1;
            }
            else if (point.X > x2 - 5 && point.X < x2 + 5 && point.Y > y2 - 5 && point.Y < y2 + 5)
            {
                return 4;
            }
            else if (point.X > x3 - 5 && point.X < x3 + 5 && point.Y > y3 - 5 && point.Y < y3 + 5)
            {
                return 2;
            }
            else if (point.X > x4 - 5 && point.X < x4 + 5 && point.Y > y4 - 5 && point.Y < y4 + 5)
            {
                return 3;
            }
            else return 0;
        }


        public override void drawCircle(Graphics g)
        {
            float x1 = this.X + (this.Width / 2) - 4;
            float y1 = this.Y - 4;
            float x2 = this.X - 4;
            float y2 = this.Y + (this.Height / 2) - 4;
            float x3 = this.X + this.Width - 4;
            float y3 = this.Y + (this.Height / 2) - 4;
            float x4 = this.X + (this.Width / 2) - 4;
            float y4 = this.Y + this.Height - 4;
            Rectangle rect1 = new Rectangle((int)x1, (int)y1, 8, 8);
            Rectangle rect2 = new Rectangle((int)x2, (int)y2, 8, 8);
            Rectangle rect3 = new Rectangle((int)x3, (int)y3, 8, 8);
            Rectangle rect4 = new Rectangle((int)x4, (int)y4, 8, 8);
            Pen pen = new Pen(Color.Black, 1);
            g.DrawEllipse(pen, rect1);
            g.DrawEllipse(pen, rect2);
            g.DrawEllipse(pen, rect3);
            g.DrawEllipse(pen, rect4);
        }

        public override void Accept(Visitor visitor,Graphics g)
        {
            visitor.Visit(this,g);
        }

        public override void fill(Graphics g,Color color)
        {
            g.FillRectangle(new SolidBrush(color),this.X, this.Y, this.Width, this.Height);
            this.drawString(g);
        }
    }
}
