﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace work1
{
    [Serializable]
    public class Parallelogram : BaseGrapthic
    {
        private Point p1;
        private Point p2;
        private Point p3;
        private Point p4;
        private Point point1;
        private Point point2;
        private Point point3;
        private Point point4;
        private bool isSelected;
        private List<Line> lineList;
        public Parallelogram()
        {
            p1.X = 40;
            p1.Y = 60;
            p2.X = 130;
            p2.Y = 60;
            p3.X = 110;
            p3.Y = 120;
            p4.X = 20;
            p4.Y = 120;
            this.point1.X = this.p1.X + 35;
            this.point1.Y = this.p1.Y;
            this.point2.X = this.p3.X + 10;
            this.point2.Y = this.P2.Y + 30;
            this.point3.X = this.P4.X + 55;
            this.point3.Y = this.p3.Y;
            this.point4.X = this.P4.X + 10;
            this.point4.Y = this.P1.Y + 30;
            isSelected = false;
            this.LineList = new List<Line>();
        }

        public override Point Point1 { get => point1; set => point1 = value; }
        public override Point Point2 { get => point2; set => point2 = value; }
        public override Point Point3 { get => point3; set => point3 = value; }
        public override Point Point4 { get => point4; set => point4 = value; }
        public bool IsSelected { get => isSelected; set => isSelected = value; }
        public override List<Line> LineList { get => lineList; set => lineList = value; }
        public Point P1 { get => p1; set => p1 = value; }
        public Point P2 { get => p2; set => p2 = value; }
        public Point P4 { get => p4; set => p4 = value; }
        public Point P3 { get => p3; set => p3 = value; }

       

        public override void add()
        {
            throw new NotImplementedException();
        }

        public override bool contains(int x, int y)
        {
            if (x > this.P1.X && x < this.P3.X && y > this.P1.Y && y < this.P3.Y)
            {
                return true;
            }
            else return false;
        }

        public override bool contains(Point point)
        {
            if (point.X > this.P1.X && point.X < this.P3.X && point.Y > this.P1.Y && point.Y < this.P3.Y)
            {
                return true;
            }
            else return false;
        }

        public override void draw(Graphics g)
        {
            Pen p = new Pen(Color.Black, 2);
            Point[] points= { p1, p2, p3, p4 };
            g.DrawPolygon(p, points);
            p.Dispose();
            g.Dispose();
        }


        public override void drawCircle(Graphics g)
        {
            float x1 = this.P1.X + 31;
            float y1 = this.p1.Y - 4;
            float x2 = this.p2.X - 14;
            float y2 = this.P1.Y + 25;
            float x3 = this.P4.X + 51;
            float y3 = this.p4.Y - 4;
            float x4 = this.p4.X + 6;
            float y4 = this.P1.Y + 25;
            Rectangle rect1 = new Rectangle((int)x1, (int)y1, 8, 8);
            Rectangle rect2 = new Rectangle((int)x2, (int)y2, 8, 8);
            Rectangle rect3 = new Rectangle((int)x3, (int)y3, 8, 8);
            Rectangle rect4 = new Rectangle((int)x4, (int)y4, 8, 8);
            Pen pen = new Pen(Color.Black, 1);
            g.DrawEllipse(pen, rect1);
            g.DrawEllipse(pen, rect2);
            g.DrawEllipse(pen, rect3);
            g.DrawEllipse(pen, rect4);
        }

        public override int isConnected(int x, int y)
        {
            float x1 = this.p1.X + 31;
            float y1 = this.p1.Y - 4;
            float x2 = this.p2.X - 14;
            float y2 = this.P1.Y + 25;
            float x3 = this.P4.X + 51;
            float y3 = this.p4.Y - 4;
            float x4 = this.p4.X + 6;
            float y4 = this.P1.Y + 25;
            if (x > x1 - 7 && x < x1 +7 && y > y1 - 7 && y < y1 + 7)
            {
                return 1;
            }
            else if (x > x2 - 7 && x < x2 + 7 && y > y2 - 7 && y < y2 + 7)
            {
                return 2;
            }
            else if (x > x3 - 7 && x < x3 + 7 && y > y3 - 7 && y < y3 + 7)
            {
                return 3;
            }
            else if (x > x4 - 7 && x < x4 + 7 && y > y4 - 7 && y < y4 + 7)
            {
                return 4;
            }
            else return 0;
        }

        public override int isConnected(Point point)
        {
            float x1 = this.P1.X + 31;
            float y1 = this.p1.Y - 4;
            float x2 = this.p2.X - 14;
            float y2 = this.P1.Y + 25;
            float x3 = this.P4.X + 51;
            float y3 = this.p4.Y - 4;
            float x4 = this.p4.X + 6;
            float y4 = this.P1.Y + 25;
            if (point.X > x1 - 7 && point.X < x1 + 7 && point.Y > y1 - 7 && point.Y < y1 + 7)
            {
                return 1;
            }
            else if (point.X > x2 - 7 && point.X < x2 + 7 && point.Y > y2 - 7 && point.Y < y2 + 7)
            {
                return 2;
            }
            else if (point.X > x3 - 7 && point.X < x3 + 7 && point.Y > y3 - 7 && point.Y < y3 + 7)
            {
                return 3;
            }
            else if (point.X > x4 - 7 && point.X < x4 + 7 && point.Y > y4 - 7 && point.Y < y4 + 7)
            {
                return 4;
            }
            else return 0;
        }

        public override void remove()
        {
            throw new NotImplementedException();
        }
        public override void Accept(Visitor visitor, Graphics g)
        {
            visitor.Visit(this,g);
        }

        public override void fill(Graphics g, Color color)
        {
            Point[] points = { p1, p2, p3, p4 };
            g.FillPolygon(new SolidBrush(color), points);
            this.drawString(g);
        }
    }
}
